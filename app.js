const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const multer = require("multer");
const port = 3000;
const route = require("./src/app/routes/index");
const db = require("./src/config/db");
const dotenv = require("dotenv");
const cors = require("cors");
const path = require("path");
const Stytch = require("stytch");
const stytchClient = new Stytch.Client({
  project_id: "project-test-e7343aff-3ebd-4236-88a6-19a904eb6fa7",
  secret: "secret-test-55ZJDIeLCreEKv3tLwQCZ_h8r1RS2hL04yk=",
});

dotenv.config();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
db.connect();

app.use("/uploads", express.static(path.join(__dirname, "uploads")));
app.post("/send-otp", async (req, res) => {
  const { email } = req.body;

  try {
    const sendOtpResponse = await stytchClient.otps.email.loginOrCreate({
      email: email,
    });

    res.json({ message: "OTP sent successfully!", data: sendOtpResponse });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to send OTP" });
  }
});
app.post("/verify-otp", async (req, res) => {
  try {
    const { method_id, code } = req.body;
    const verifyOtpResponse = await stytchClient.otps.authenticate({
      method_id,
      code,
    });
    console.log(verifyOtpResponse);
    res.json({ message: "OTP verified successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to verify OTP" });
  }
});
route(app);
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
