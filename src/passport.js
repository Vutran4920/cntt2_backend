const GoogleStrategy = require("passport-google-oauth20").Strategy;
require('dotenv').config()
const passport = require("passport");

passport.use(new GoogleStrategy({
    authorizationURL: 'http://localhost:3000/auth/google/signin',
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: "http://localhost:3000/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    User.findOrCreate({ UserId: profile.id }, function (err, user) {
      return cb(err, user);
    });
  }
));

module.exports = passport;