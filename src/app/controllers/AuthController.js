const bcrypt = require("bcryptjs");
const User = require("../models/User");
const { generateAccessToken } = require("../middleware/user-api");
class AuthController {
  /* POST register */
  async register(req, res, next) {
    try {
      const email = req.body.email;
      const password = req.body.password;
      const fullname = req.body.fullname;
      const file = req.file;
      const useremail = await User.findOne({ email });
      if (!email || !password || !fullname)
        return res
          .status(500)
          .json({ content: "Xin hãy điền đầy đủ thông tin" });
      if (useremail) {
        return res.status(500).json({ content: "Email đã tồn tại" });
      } else {
        bcrypt.genSalt(10, function (err, salt) {
          bcrypt.hash(password, salt, async function (err, hash) {
            const newuser = await User.create({
              ...req.body,
              avatar: file ? file.path : "",
              password: hash,
            });
            const token = generateAccessToken({ UserId: newuser._id });
            return res.status(200).json({
              data: newuser,
              token: token,
            });
          });
        });
      }
    } catch (err) {
      return res.status(400).json(err);
    }
  }

  async login(req, res, next) {
    try {
      const email = req.body.email;
      const password = req.body.password;
      if (!email || !password)
        return res
          .status(500)
          .json({ content: "Vui lòng nhập tài khoản hoặc mật khẩu" });
      const user = await User.findOne({ email });
      if (!user)
        return res.status(400).json({ content: "Sai email hoặc mật khẩu!" });
      bcrypt.compare(password, user.password, async (err, result) => {
        if (result) {
          const token = generateAccessToken({ UserId: user._id });
          const findUser = await User.findById(user._id).select(
            "password role"
          );
          return res.status(200).json({
            data: findUser,
            token: token,

            content: "Đăng nhập thành công!",
          });
        } else {
          return res.status(400).json({ content: "Mật khẩu không đúng!" });
        }
      });
    } catch (err) {
      return res.status(400).json(err);
    }
  }
}
module.exports = new AuthController();

// /* POST login */
// exports.login = async (req, res, next) => {
//     const email = req.body.email;
//     const password = req.body.password;

//     if (!email || !password)
//       return res.status(500).json({ content: "Please enter email or password" });

//     User.find({ email:email })
//       .populate("cart")
//       .exec((err, users) => {
//           if (err) return res.status(500).json(err);
//           if (users.length === 1) {
//           const user = users[0];

//           bcrypt.compare(password, user.password, async (err, result) => {
//               if (result) {
//               const token = generateAccessToken({ userId: user._id });
//               const findUser = await User.findById(user._id).select("password")

//               return res.json({
//                   data: findUser,
//                   token: token
//               });
//               } else {
//               return res.status(404).json({ content: "Invalid email or password!" });
//               }
//           });
//           } else {
//           return res.status(404).json({ content: "Invalid email or password!" });
//           }
//         });
//   };
