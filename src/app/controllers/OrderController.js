const Order = require("../models/Order");
class OrderController {
  /* GET all orders */
  async getAllOrders(req, res, next) {
    try {
      const orders = await Order.find({})
        .sort({ createdAt: -1 })
        .populate("services");
      return res.status(200).json(orders);
    } catch (err) {
      console.log(err);
      return res.status(400).json(err);
    }
  }
  // Get all orders of providerId
  async getAllOrdersOfUser(req, res, next) {
    try {
      const userId = req.params.id;
      const orders = await Order.find({
        createdBy: userId,
      })
        .sort({ createdAt: -1 })
        .populate("services", "servicesName thumbnail");
      return res.status(200).json(orders);
    } catch (err) {
      console.log(err);
      return res.status(400).json(err);
    }
  }

  async getAllOrdersOfProvider(req, res, next) {
    try {
      const userId = req.params.id;
      const ObjectId = require("mongoose").Types.ObjectId;
      const convertedUserId = new ObjectId(userId);
      const orders = await Order.find({})
        .sort({ createdAt: -1 })
        .populate("services");

      const filteredOrders = orders.filter((order) => {
        // Check if any service in the order has createdBy matching userId
        return order.services.some((service) =>
          service.createdBy.equals(convertedUserId)
        );
      });
      return res.status(200).json(filteredOrders);
    } catch (err) {
      console.log(err);
      return res.status(400).json(err);
    }
  }
  /* GET an order */
  async getAnOrder(req, res, next) {
    try {
      const order = await Order.findById(req.params.id).populate("services");
      return res.status(200).json(order);
    } catch (err) {
      console.log(err);
      return res.status(400).json(err);
    }
  }

  /* PUT order */
  async putOrder(req, res, next) {
    try {
      const order = await Order.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
      }).populate("services");
      return res.status(200).json(order);
    } catch (err) {
      return res.stautus(400).json(err);
    }
  }

  /* DELETE order */
  async delOrder(req, res, next) {
    try {
      await Order.findByIdAndDelete(req.params.id);
      return res.json("Order Deleted");
    } catch (err) {
      return res.status(400).json(err);
    }
  }

  /* POST order */
  async postOrder(req, res, next) {
    try {
      const neworder = await Order.create({ ...req.body });
      console.log(neworder);
      return res.status(201).json(neworder);
    } catch (err) {
      return res.status(400).json(err);
    }
  }
}

module.exports = new OrderController();
