const Services = require("../models/Sevices");
const { transformSearchTextRegexp } = require("../helper");
class ServicesController {
  //[GET] get all services of provider base on provider_id
  async showByProvider(req, res) {
    try {
      const services = await Services.find({
        createdBy: req.params.id,
      });
      res.json(services);
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  }
  
  //[GET] get all services for users
  async showByUser(req, res) {
    try {
      const { search } = req.query;
      const { area } = req.query;

      let newquery = {};
      //Search service by Service name
      if (search) {
        // Remove spaces and then split the search string into individual characters
        const characters = search.replace(/\s+/g, "").split("");

        // Construct a regex pattern that matches the characters in sequence, allowing for any characters in between
        const pattern = characters
          .map((char) => transformSearchTextRegexp(char))
          .join(".*");
        const regexSearch = new RegExp(pattern, "i");
        console.log(`Regex used for search: ${regexSearch}`);

        newquery = { $or: [{ servicesName: { $regex: regexSearch } }] };
      }

      if (area) {
        // Remove spaces and then split the search string into individual characters
        const characters = area.replace(/\s+/g, "").split("");

        // Construct a regex pattern that matches the characters in sequence, allowing for any characters in between
        const pattern = characters
          .map((char) => transformSearchTextRegexp(char))
          .join(".*");
        const regexSearch = new RegExp(pattern, "i");
        console.log(`Regex used for search: ${regexSearch}`);

        newquery = { $or: [{ area: { $regex: regexSearch } }] };
      }

      const services = await Services.find(newquery).collation({
        locale: "vi",
        strength: 1,
      });

      res.json(services);
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  }

  //[GET] get service detail by id
  async showById(req, res) {
    try {
      const service = await Services.findById(req.params.id);
      res.json({
        ok: true,
        service: service,
        msg: `Lấy dịch vụ với ${req.params.id} thành công`,
      });
    } catch (err) {
      res.status(400).json("Dịch vụ không tồn tại");
    }
  }
  //[POST] add new service for provider
  async store(req, res) {
    // const providerId = req.params.provider_id;
    // console.log(providerId);
    // // let setproviderId = req.body.providerId;
    // console.log(req.body);
    const file = req.file;

    try {
      const service = new Services({
        ...req.body,
        thumbnail: file ? file.path : "",
      });
      console.log(service);
      await service.save();
      res.json(service);
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  }
  //[Update] update service by id
  async update(req, res) {
    const file = req.file;
    if (file) {
      req.body.thumbnail = file.path;
    }
    try {
      const service = await Services.findByIdAndUpdate(
        req.params.id,
        req.body,

        { new: true }
      );

      res.json(service);
    } catch (err) {
      res.status(400).json("error: err.message");
    }
  }
  //[Delete] delete service by id
  async delete(req, res) {
    try {
      await Services.findByIdAndDelete(req.params.id);
      res.json("Service deleted");
    } catch (err) {
      res.status(400).json("error: err.message");
    }
  }
}
module.exports = new ServicesController();
