const nodemailer = require("nodemailer");
const User = require("../models/User"); // Adjust the path as necessary

// Configure Nodemailer
const transporter = nodemailer.createTransport({
  service: "gmail", // Use your email service provider
  auth: {
    user: "zedpro123456788@gmail.com", // Your email address
    pass: "mchg htet aapg hway", // Your email password or app-specific password
  },
});

class EmailNotificationController {
  // Get all users email with role user
  async getAllUserEmail(req, res, next) {
    try {
      const users = await User.find({ role: "user" }).select("email");
      return res.status(200).json(users);
    } catch (err) {
      console.log(err);
      return res.status(400).json(err);
    }
  }

  // Function to get all user emails with role "user"
  async getAllUserEmails() {
    try {
      const users = await User.find({ role: "user" }).select("email");
      return users.map((user) => user.email);
    } catch (error) {
      console.error("Error fetching user emails:", error);
      throw error;
    }
  }

  // Send emails to all users with role "user"
  async sendEmails(req, res, next) {
    try {
      const { userEmail, orderID } = req.body;
      if (!userEmail) {
        return res.status(400).send("User email is required");
      }
      const mailOptions = {
        from: '"Thông báo hệ thống từ hệ thống" <zedpro123456788@gmail.com>',
        to: userEmail,
        subject: "Thông báo cập nhật đơn hàng",
        text: `Đơn hàng  ${orderID} đã được cập nhật trạng thái khách hàng hãy kiểm tra lại thông tin đơn hàng của mình`,
      };
      await transporter.sendMail(mailOptions);
      console.log(`Email sent to ${mailOptions.to}`);
      // for (const email of emails) {
      //   mailOptions.to = email;
      //   await transporter.sendMail(mailOptions);
      //   console.log(`Email sent to ${email}`);
      // }

      return res.status(200).send("Emails sent successfully");
    } catch (error) {
      console.error("Error sending emails:", error);
      return res.status(500).send("Error sending emails");
    }
  }
}

module.exports = new EmailNotificationController();
