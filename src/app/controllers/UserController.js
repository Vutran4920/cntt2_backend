const User = require("../models/User");
class UserController {
  /* GET all users */
  async getAllUsers(req, res, next) {
    try {
      const users = await User.find({});
      return res.status(200).json(users);
    } catch (err) {
      return res.status(400).json(err);
    }
  }

  /* GET a user */
  async getSingleUser(req, res, next) {
    try {
      const user = await User.findById({
        _id: req.params.id,
      });
      return res.status(200).json(user);
    } catch (err) {
      return res.status(400).json(err);
    }
  }

  /* PUT user */
  async putUser(req, res, next) {
    const file = req.file;
    console.log(file);
    if (file) {
      req.body.avatar = file.path;
    }
    try {
      const user = await User.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
      });
      return res.status(200).json(user);
    } catch (err) {
      return res.status(400).json(err);
    }
  }

  /*DELETE user */
  async deleteUser(req, res, next) {
    try {
      await User.findByIdAndDelete(req.params.id);
      return res.json("User Deleted");
    } catch (err) {
      return res.status(400).json(err);
    }
  }
}
module.exports = new UserController();
