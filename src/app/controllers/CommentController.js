const Services = require("../models/Sevices");
const Comments = require("../models/Comments");
const Users = require("../models/User");

const userId = "666fcbad3755692d823f4ec9";
class CommentController {
  // Show all comments of a service base on service_id
  async showByService(req, res) {
    try {
      const comments = await Comments.find({
        servicesId: req.params.id,
      }).populate("user", "fullname avatar");
      res.json(comments);
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  }

  // Create a new comment for a service
  async create(req, res) {
    try {
      const { rating, content, servicesId, userId } = req.body;
      const user = await Users.findById(userId);
      if (!user) {
        return res.status(404).json({ error: "Không thấy người dùng" });
      }
      const services = await Services.findById(servicesId);
      if (!services) {
        return res.status(404).json({ error: "Không thấy dịch vụ" });
      }
      const newComment = new Comments({
        user: {
          userId: user._id,
          fullname: user.fullname,
          avatar: user.avatar,
        },
        rating,
        content,
        servicesId,
      });

      await newComment.save();
      // Add comment to services array
      services.comments.push(newComment);
      await services.save();
      res.json(newComment);
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  }
  // update a comment
  async update(req, res) {
    try {
      const { content, rating } = req.body;
      const comment = await Comments.findByIdAndUpdate(
        req.params.id,
        {
          content,
          rating,
        },
        { new: true }
      );
      res.json(comment);
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  }
  // delete a comment
  async delete(req, res) {
    try {
      // get service by id
      const services = await Services.findById(req.body.servicesId);

      // get comment by id
      const commentId = req.params.id;

      // find index of comment in services array
      const serviceIndex = services.comments.findIndex(
        (c) => c._id === commentId
      );
      // delete comment
      await Comments.findByIdAndDelete(commentId);
      // delete comment in services array
      services.comments.splice(serviceIndex, 1);
      await services.save();
      res.json({
        ok: true,
        msg: "Xoá bình luận thành công",
      });
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  }
}
module.exports = new CommentController();
