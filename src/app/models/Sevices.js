const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Services = new Schema({
  servicesName: { type: String, required: true, maxLenght: 50 },
  description: { type: String, required: true },
  thumbnail: { type: String, default: "" },
  price: { type: Number, required: true },
  area: { type: String, required: true },
  exp: { type: String, required: true, maxLenght: 250 },
  createdBy: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: "comments" }],
});
module.exports = mongoose.model("services", Services);
