const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CommentSchema = new Schema(
  {
    servicesId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "services",
    },
    content: { type: String, required: true },
    rating: { type: Number, required: true },
    user: { type: Object, default: null },
  },
  { timestamps: true }
);
module.exports = mongoose.model("comments", CommentSchema);
