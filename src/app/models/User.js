const mongoose = require("mongoose");

const UserSchema = mongoose.Schema(
  {
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    fullname: { type: String, required: true },
    address: { type: String, required: true },
    role: { type: String, default: "user" },
    phonenumber: { type: String, required: true, default: 0 },
    avatar: { type: String, default: "" },
  },
  { timestamps: true }
);

const User = mongoose.model("user", UserSchema);

module.exports = User;
