const mongoose = require('mongoose');
const CategorySchema = mongoose.Schema({
    name:{ type:String, required: true},
    image:{ type: String, default: ""},
}, { timestamps: true});

const Category = mongoose.model('category', CategorySchema);
module.exports = Category;