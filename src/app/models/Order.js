const mongoose = require("mongoose");
const { required } = require("nodemon/lib/config");
const { add } = require("nodemon/lib/rules");

const OrderSchema = mongoose.Schema(
  {
    name: { type: String, required: true },
    address: { type: String, required: true },
    phone: { type: String, required: true },
    bookingDate: { type: Date, default: Date.now },
    note: { type: String, default: "" },
    status: { type: String, required: true, default: "pending" },
    services: [{ type: mongoose.Schema.Types.ObjectId, ref: "services" }],
    price: { type: Number, required: true, default: 0 },
    userEmail: { type: String, required: true },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
  },
  { timestamps: true }
);

const Order = mongoose.model("order", OrderSchema);
module.exports = Order;
