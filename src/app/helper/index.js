// const transformSearchTextRegexp = (text) => {
//   const specialCharacters = [
//     "aàáãảạăằắẳẵặâầấẩẫậ",
//     "eèéẻẽẹêềếểễệ",
//     "dđ",
//     "uùúủũụưừứửữự",
//     "oòóỏõọôồốổỗộơờớởỡợ",
//     "iìíỉĩị",
//     "yýỳỹỵỷ",
//   ];

//   return text
//     .toLocaleLowerCase()
//     .split("")
//     .map((item) => {
//       const specialChar = specialCharacters.find((sc) => sc.includes(item));
//       if (!specialChar) return item;
//       return `[${specialChar.split("").join(",")}]`;
//     })
//     .join("");
// };
const transformSearchTextRegexp = (text) => {
  // Escape regex special characters except for the ones we'll handle separately
  const escapeRegExp = (string) =>
    string.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");

  const specialCharacters = [
    "aàáãảạăằắẳẵặâầấẩẫậ",
    "eèéẻẽẹêềếểễệ",
    "dđ",
    "uùúủũụưừứửữự",
    "oòóỏõọôồốổỗộơờớởỡợ",
    "iìíỉĩị",
    "yýỳỹỵỷ",
  ];

  return escapeRegExp(text)
    .toLocaleLowerCase()
    .split("")

    .map((item) => {
      // Replace spaces with regex for one or more whitespace characters
      const specialChar = specialCharacters.find((sc) => sc.includes(item));
      if (!specialChar) return item;
      return `[${specialChar.split("").join("")}]`; // No need to separate characters with commas
    })

    .join("");
};
module.exports = { transformSearchTextRegexp };
