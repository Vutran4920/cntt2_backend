const express = require("express");
const router = express.Router();
const Order = require("../models/Order");
const OrderController = require("../controllers/OrderController");
const { authenticateToken } = require("../middleware/user-api");

router.use(authenticateToken);

/* GET an order */
router.get("/:id", OrderController.getAnOrder);

/* GET all orders */
router.get("/", OrderController.getAllOrders);
//
router.get("/user/:id", OrderController.getAllOrdersOfUser);
router.get("/provider/:id", OrderController.getAllOrdersOfProvider);
/* POST order */
router.post("/", OrderController.postOrder);

/* PUT order */
router.put("/:id", OrderController.putOrder);

/* DELETE order */
router.delete("/:id", OrderController.delOrder);

module.exports = router;
