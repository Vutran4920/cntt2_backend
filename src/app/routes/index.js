const servicesRoute = require("./services");
const authRoute = require("./auth");
const userRoute = require("./user");
const orderRoute = require("./order");
const commentRoute = require("./comments");
const notificationRoute = require("./notification");
const express = require("express");
const path = require("path");
const paymentRoute = require("./momo");
function route(app) {
  app.get("/", (req, res) => {
    res.send("Hello World!");
  });

  app.use("/services", servicesRoute);
  app.use("/auth", authRoute);
  app.use("/user", userRoute);
  app.use("/order", orderRoute);
  app.use("/comment", commentRoute);
  app.use("/momo", paymentRoute);
  app.use("/notification", notificationRoute);
}
module.exports = route;
