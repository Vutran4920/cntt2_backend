const express = require("express");
const router = express.Router();
const servicesController = require("../controllers/ServicesController");
const upload = require("../middleware/upload");
// route delete service
router.delete("/:id", servicesController.delete);
// route update service
router.put("/:id", upload.single("thumbnail"), servicesController.update);
// route add new service
router.post("/", upload.single("thumbnail"), servicesController.store);
//route get service by id
router.get("/:id", servicesController.showById);
//route get all services
router.get("/", servicesController.showByUser);
//route get all services of provider
router.get("/provider/:id", servicesController.showByProvider);

module.exports = router;
