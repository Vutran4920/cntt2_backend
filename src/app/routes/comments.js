const express = require("express");
const router = express.Router();
const commentsController = require("../controllers/CommentController");

router.delete("/:id", commentsController.delete);
router.put("/:id", commentsController.update);
router.post("/", commentsController.create);
router.get("/:id", commentsController.showByService);

module.exports = router;
