const express = require("express");
const router = express.Router();
const emailNotifi = require("../controllers/EmailNoftificationController");

router.get("/", emailNotifi.getAllUserEmail);
router.post("/sendEmails", emailNotifi.sendEmails.bind(emailNotifi));
module.exports = router;
