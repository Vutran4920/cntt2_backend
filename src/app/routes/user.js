const express = require("express");
const router = express.Router();
const User = require("../models/User");
const UserController = require("../controllers/UserController");
const { authenticateToken } = require("../middleware/user-api");
const upload = require("../middleware/upload");

/* GET a user */
router.get("/:id", UserController.getSingleUser);

/* GET all users */
router.get("/", UserController.getAllUsers);

router.use(authenticateToken);

/* PUT user */
router.put("/:id", upload.single("image"), UserController.putUser);

/* DELETE user */
router.delete("/:id", UserController.deleteUser);

module.exports = router;
