const express = require("express");
const router = express.Router();
const User = require("../models/User");
const AuthController = require("../controllers/AuthController");
const { authenticateToken } = require("../middleware/user-api");
const upload = require("../middleware/upload");
const passport = require("../../passport");
router.use(authenticateToken);
/* POST register */
router.post("/signup", upload.single("image"), AuthController.register);
/* POST login */
router.post("/signin", AuthController.login);
/* GOOGLE */

router.get("/google/signin", passport.authenticate('google', { scope: ['profile'] }))
router.get('/google/callback', 
    passport.authenticate('google', { failureRedirect: '/login' }),
    function(req, res) {
      // Successful authentication, redirect home.
      res.redirect('/');
    });

module.exports = router;
